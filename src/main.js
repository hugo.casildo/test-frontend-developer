import Vue from 'vue'
import App from './App.vue'
import { currency } from "@/core/utils/currency";

Vue.config.productionTip = false
Vue.filter('currency', currency)

new Vue({
  render: h => h(App),
}).$mount('#app')
