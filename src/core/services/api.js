const URL = process.env.VUE_APP_API_PATH

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

function getItems() {

  return fetch (`${URL}/demopayload`, {
    method: 'GET',
    //body: JSON.stringify(body),
    mode: 'cors',
    headers
  })
    .then(resp => resp.json())
}

export default {
  getItems
}
