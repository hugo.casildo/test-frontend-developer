const filters = [
  {
    id: 'order',
    name: 'Mostrar/Ocultar',
    values: [
      { name: 'Nombre', value: 'name'},
      { name: 'Categoria', value: 'category' },
      { name: 'Precio', value: 'price' },
    ]
  },
  {
    id: 'show',
    name: 'Mostrar/Ocultar sin Stock',
    values: [
      { name: 'Mostrar', value: 'show'},
      { name: 'Ocultar', value: 'hidden'}
    ]
  },
  {
    id: 'category',
    name: 'Categoria',
    values: [
      { name: 'Papel', value: 'paper'},
      { name: 'Herramientas', value: 'tools'}
    ]
  },
]

export default filters;
