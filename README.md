# Prueba de ecommerce de Hugo Casildo


## Clonar Repositorio
```
git clone https://gitlab.com/hugo.casildo/test-frontend-developer.git
```


## Entrar al proyecto
```
cd test-frontend-developer
```

## Instalar dependencias
```
npm install
```

### Iniciar el servidor de desarrollo
```
npm run serve
```

### Compilar versión para producción
```
npm run build
```

### Puedes visitar el proyecto desplegado en la siguiente URL
Ir [Test Hugo Casildo](https://test-hugo-casildo.netlify.app).
